<? /*Template Name: Список тестов*/ 

get_header();
?>
<h2>Список тестов:</h2>
<?
	//запрос к базе за данными
	$posts = get_posts( array(
          'numberposts' => '100', //число возвращаемых объектов              
          'category' => '', 
          'orderby' => 'post_date',
          'order' => 'DESC',              
          'post_type' => 'page', //тип материала – page – страница, post - запись                            
          'post_status' => 'publish',
          'post_parent' => '5'
	) );

	//var_dump($posts);
?>

<?
	//в цикле выводим нужное количество заполненных кусков html-разметки
	foreach($posts as $post):    
    	$postid = $post->ID; //id текущей записи из выбираемых
?>

    <a href="<?=get_permalink($postid);?>"> <?=$post->post_title;?> </a>

	<?endforeach;?>	    

<? get_footer();?>
