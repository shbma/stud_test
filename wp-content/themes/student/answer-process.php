<?
/*Template Name: Список вопросов*/

get_header();

//получаем и запоминаем ID текущей записи-страницы
while ( have_posts() ) : the_post();
    $postID = get_the_ID();
endwhile;

?>

<h2>Тест "<?the_title();?>":</h2>
<?
	//запрос к базе за данными
	$posts = get_posts( array(
          'numberposts' => '100', //число возвращаемых объектов              
          'category' => '', 
          'orderby' => 'post_date',
          'order' => 'DESC',              
          'post_type' => 'page', //тип материала – page – страница, post - запись                            
          'post_status' => 'publish',
          'post_parent' => $postID
	) );

	//var_dump($posts);
?>
<form method=POST>
	<?
	//в цикле выводим вопросы
	foreach($posts as $post):    
    	$postid = $post->ID; //id текущей записи из выбираемых
	?>

	<div class="question-block">
		Вопрос:
		<div class="question-content"> <?=$post->post_content;?> </div>
		<input type="text" name="<?=$postid?>">
	</div>

	<?endforeach;?>	 
</form>

<? get_footer();?>