<?
/*Template Name: Итоги тестов*/

require('header.php');

global $wpdb;

$userID = $current_user->ID;
$user_roles = $current_user->roles;

if (in_array('administrator',$user_roles )) {

	//достаем имена и id-шники пользователей, проходивших тесты когда-либо
	$users = $wpdb->get_results('
		SELECT u.id, u.display_name 
		FROM 
			wp_users AS u, 
			(SELECT DISTINCT user_id FROM wp_users_tests) AS ut 
		WHERE u.id = ut.user_id 
	');

	//для каждого пользователя смотрим пройденные тесты
	foreach ($users as $k => $user) {?>
		<div class="user-block">
			<div class="user-info"><?=$user->display_name?></div>
			<div class="test-results">
				<?
				//запрос какие тесты пройдены
				$tests_info = $wpdb->get_results('
					SELECT test_id, date 
					FROM wp_users_tests 
					WHERE user_id = '.$user->id.'
					ORDER BY date DESC
					
					LIMIT 1');
				
				//перебираем пройденные тесты
				foreach ($tests_info as $k => $test_info) :
					$test = get_post($test_info->test_id);
					?>
					<div class="test-res">
						<div class="test-title"><?=$test->post_title?> (<?=$test_info->date?>)</div>
						<div class="test-questions">
							<?
							//вопросы
							$questions = get_posts( array(
								'numberposts' => '100', //число возвращаемых объектов              
								'category' => '', 
								'orderby' => 'post_date',
								'order' => 'DESC',              
								'post_type' => 'page', //тип материала – page – страница, post - запись                            
								'post_status' => 'publish',
								'post_parent' => $test->ID
							) );

							//собираем ответы
							$score = 0;
							foreach ($questions as $k => $question) :
								$qa = $wpdb->get_row('
									SELECT mark 
									FROM wp_users_tests 
									WHERE 
										user_id = '.$user->id.' 
										AND test_id='.$test->ID.' 
										AND question_id = '.$question->ID.'
								'); 
								$score += $qa->mark;
								?>

								<div class="inres-question">
									<div class="inres-question-title">
										<?=$question->post_title?>
									</div>
									<div class="inres-question-mark">
										<?=$qa->mark?>
									</div>
								</div>
							<?endforeach;?>
						</div>
						<div class="test-itog">
							Итог: <? echo round( 100*$score/count($questions), 0); ?>% (<?=$score?> из <?=count($questions)?>)
						</div>
					</div>	
				<?endforeach;	?>				
			</div>
		</div>
	<?}



} else { ?>
	<h2>К сожалению, у Вас недостаточно прав для просмотра этого материала.</h2>
<?}?>
