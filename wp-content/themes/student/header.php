<!DOCTYPE html>
<html lang="en">
<head>
	<base href="<?bloginfo('url')?>">
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="<?bloginfo("url");?>/wp-content/themes/student/style.css">
	<script type="text/javascript" src="<?bloginfo("url");?>/wp-content/themes/student/js/jquery-2.2.min.js"></script>
</head>
<body>
	<?php
    $current_user = wp_get_current_user();
    /**
     * @example Safe usage: $current_user = wp_get_current_user();
     * if ( !($current_user instanceof WP_User) )
     *     return;
     */
    ?>
    <div class="public_user_info">
    	Вы вошли как: <?=$current_user->user_login;?>
    	<?
    	$first_name = $current_user->user_firstname;
    	$last_name = $current_user->user_lastname;
    	$user_roles = $current_user->roles;
    	if ($first_name || $last_name) {
    		echo "(" . $first_name . " " . $last_name . ")";
    	}
    	//var_dump(get_user_meta($current_user->ID));
    	
    	//достаем группы пользователя
    	$groups = wp_get_object_terms($current_user->ID, 'user-group');
    	$user_groups = Array();
    	foreach ($groups as $k => $group_obj) {
    		$user_groups[] = $group_obj->slug;
    	}
    	//var_dump($user_groups);
    	?>
    </div>
    <!-- echo 'User ID: ' . $current_user->ID . '<br />'; -->
	