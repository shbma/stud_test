<?
/*Template Name: Список вопросов*/

require('header.php');

//получаем и запоминаем ID текущей записи-страницы
while ( have_posts() ) : the_post();
    $postID = get_the_ID();
endwhile;

//достаем категории
$categories = wp_get_object_terms($postID, 'category');

// var_dump($categories);
// var_dump($user_groups);
// var_dump($user_roles);

//сравним с группами пользователя, чтобы показать страницу или нет
$accessible = false;
if (count($user_roles)){ //залогиненный пользователь
	foreach ($categories as $k => $cat_obj) {
		if ($cat->slug == $user_groups->slug) {
			$accessible = true;
			break;
		};
	}
}

//сделать потом - чтобы сравнивалась не любое попадаение, а четко
//сочетание тех же категорий - как у студента

//admin видит все
if (in_array('administrator',$user_roles )) $accessible = true;
?>

<?if ($accessible) :?>

	<h2>Тест "<?the_title();?>":</h2>
	<?
	//запрос к базе за данными
	$posts = get_posts( array(
          'numberposts' => '100', //число возвращаемых объектов              
          'category' => '', 
          'orderby' => 'post_date',
          'order' => 'DESC',              
          'post_type' => 'page', //тип материала – page – страница, post - запись                            
          'post_status' => 'publish',
          'post_parent' => $postID
	) );

	//var_dump($posts);
	?>
	<form id="answers_form" method="POST">
		<?
		//в цикле выводим вопросы
		foreach($posts as $post):    
	    	$postid = $post->ID; //id текущей записи из выбираемых
		?>

		<div class="question-block">
			<div class="question-word">Вопрос:</div>
			<div class="question-content"> <?=$post->post_content;?> </div>
			<input type="text" name="qid_<?=$postid?>">
		</div>

		<?endforeach;?>

		<input type="hidden" name="test_id" value="<?=$postID?>">

		<div class="b-submit-answers">Отправить на проверку</div>
	</form>

<?else: //not-accessible?>
	<h2>К сожалению, у Вас недостаточно прав для просмотра этого материала.</h2>

<?endif; // /accessible?>

<script>
	$('.b-submit-answers').click(function(){
		$.ajax({
			url: $('base').attr('href') + "/checker", //сам на себя
			method: "POST",
			data: $(this).parent().serialize(),
			success: function(res){
				console.log(res);
				res = JSON.parse(res);

				var res_len = res.length;
				for (var i=0; i<res_len; i++){
					//выделим верные и неверные ответы
					if ( parseInt(res[i]['value']) == 1){
						$('input[name='+res[i]['name']+']').addClass('q-bingo');
					} else {
						$('input[name='+res[i]['name']+']').addClass('q-fail');
					}

					//деактивируем кнопку
					$('.b-submit-answers').off('click');
					$('.b-submit-answers').addClass('b-inactive');
					$('.b-submit-answers').html('Отправлено');
				}
			}

		});
	});
</script>

<? get_footer();?>