<?
/*Template Name: Проверяющий скрипт*/

$userID = $current_user->ID;

$test_id = intval($_REQUEST['test_id']);

if ($test_id <= 0) die('error: id of test is not specified');

//запрос к базе за вопросами заданного теста
$posts = get_posts( array(
      'numberposts' => '1000', //число возвращаемых объектов              
      'category' => '', 
      'orderby' => 'post_date',
      'order' => 'DESC',              
      'post_type' => 'page', //тип материала – page – страница, post - запись                            
      'post_status' => 'publish',
      'post_parent' => $test_id
) );

//var_dump($posts);

//в цикле выводим вопросы
$res = Array();
foreach($posts as $post){
	$postid = $post->ID; //id текущего вопроса

	foreach ($_REQUEST as $name => $answ ){ //просеиваем запрос, ищем номера вопросов
		if (strpos($name, 'qid_')+1){
			$qid = intval(end(explode('_',$name)));	
			//ловим ответ на конкретный вопрос
			if ($qid === $postid){
				$right_answ = get_post_meta($qid,'answer',true); //правильный ответ
				$answ_type = get_post_meta($qid, 'type_of_answer',true); //тип данных ответа
				$answ_decimal = intval(get_post_meta($qid, 'decimal',true)); //знаков после запятой
				$answ_uncertanity = floatval(get_post_meta($qid, 'uncertanity',true)); //допустимая погрешность
				
				//сравним ответ студента с правильным
				$check_res = isEqual($answ, $right_answ, $answ_type, $answ_decimal, $answ_uncertanity) ? 1 : 0;
				//запишем в выходной контейнер
				$res[] = Array(
					'name'=>'qid_'.$postid, 
					'value' => $check_res
					);
	
				//сохраним в базу ...
				saveToDB($userID, $test_id, $qid, $answ, $check_res);

			}
			
		}
		
	}

};

echo json_encode($res);

/**
 * определяет равны или нет $a (ответ студена) и $ra (правильный ответ)
 *
 * $type - тип данных ответа
 * $decimal - кол-во знаков после запятой, $uncertanity - процент допустимой погрешности
 *
 * возвращает true, если равно и false в противном случае 
 */
function isEqual($a, $ra, $type, $decimal, $uncertanity){
	$result = false;

	switch ($type) {
		case 'string':
			$result = (strval($a) === strval($ra));
			break;
		default: # float
		    $a = round(floatval($a), $decimal);
		    $ra = round(floatval($ra), $decimal);
		    if ( abs($a-$ra) <= $uncertanity*abs($ra) && $a*$ra >= 0 ) {
		    	$result = true;
		    }
			break;
	}

	return $result;
}

//заносим результат ответа студента на вопрос в базу
function saveToDB($userID, $test_id, $qid, $answ, $check_res){
	global $wpdb;

	$q = $wpdb->query( '
		INSERT INTO wp_users_tests (user_id, test_id, question_id, student_answer, mark, date)
		VALUES ('.$userID.', '.$test_id.', '.$qid.', '.$answ.', '.$check_res.', NOW());
		' );
 
	return $q;
}