<? /*Template Name: Фотогалерея*/?>
<?require_once('header.php');?>

<?
$post = get_post($postID); //объект и информацией по странице
?>

<div class="left">

	<div class="text1">
	<h1><? the_title(); ?></h1> 
	    <? echo $post->post_content;?>
	</div>

</div>

<div class="right">
	<div class="gallery">
		
		<?
		//запрос к базе за данными
	  	$posts = get_posts( array(
	              'numberposts' => '100', //число возвращаемых объектов              
	              'category' => '', 
	              'orderby' => 'post_date',
	              'order' => 'DESC',              
	              'post_type' => 'page', //тип материала – page – страница, post - запись                            
	              'post_status' => 'publish',
	              'post_parent' => '42'
	   	) );

	  	//var_dump($posts);
		?>

		<?
	  	//в цикле выводим нужное количество заполненных кусков html-разметки
	  	foreach($posts as $post):
		    
		    $postid = $post->ID; //id текущей записи из выбираемых

			//получаем путь к минатюре и дополняем разметку
			$image_arr = wp_get_attachment_image_src(get_post_thumbnail_id($postid), 'thumbnail');
			$image_url = $image_arr[0]; // $image_url  сторка с URL.
		?>

	        <a href="<?=$image_url;?>" data-rel="lightcase:myCollection" data-lc-categories="myCategory1 myCategory2">
		      <img src="<?=$image_url;?>" class="img-sm" title="<?=$post->post_content;?>">
		    </a>

	  	<?endforeach;?>	    
    
	</div>

	 <? 
	$img_id = get_post_meta($postID, 'logo_secondary', true );
	$figure_arr = wp_get_attachment_image_src($img_id, array(50,50));
	$fig_src = $figure_arr[0];
	?>	
    <a href="index.html">
    	<img src="<?=$fig_src?>" class="like" alt="">
    </a>

</div>


<?get_footer();?>