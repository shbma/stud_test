<? /*Template Name: Второстепенные страницы*/?>
<?require_once('header.php');?>

<?
$post = get_post($postID); //объект и информацией по странице
?>

<div class="left">

	<div class="text1">
	<h1><? the_title(); ?></h1> 
	    <? echo $post->post_content;?>
 
	    <? 
		$img_id = get_post_meta($postID, 'logo_secondary', true );
		$figure_arr = wp_get_attachment_image_src($img_id, array(50,50));
		$fig_src = $figure_arr[0];
		?>	
	    <a href="index.html">
	    	<img src="<?=$fig_src?>" class="like" alt="">
	    </a>
	</div>

</div>

<div class="right">
    <?echo get_post_meta($postID, 'right_content', true );?>   
</div>


<?get_footer();?>
